from conans import ConanFile, CMake


class AllocatorsConan(ConanFile):
    name = "allocators"
    version = "1.2"
    license = "Boost Software License - Version 1.0"
    author = "Lukasz Laszko lukaszlaszko@gmail.com"
    url = "https://bitbucket.org/lukaszlaszko/allocators"
    description = "Composite allocators for C++ 14"
    topics = ("c++", "memory", "allocators")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = ["src/*"]
    build_requires = "gtest/1.8.1@bincrafters/stable"

    def package(self):
        self.copy("*.hpp", dst="include", src="src/include")
        self.copy("*.ipp", dst="include", src="src/include")

    def package_info(self):
        self.cpp_info.libs = ["allocators"]
