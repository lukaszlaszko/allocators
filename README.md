[![Sources](https://img.shields.io/badge/bitbucket-sources-green.svg?style=flat)](https://bitbucket.org/lukaszlaszko/allocators/)
[![Pipelines](https://img.shields.io/badge/bitbucket-pipelines-blue.svg?style=flat)](https://bitbucket.org/lukaszlaszko/allocators/addon/pipelines/home#!/)
[![Documentation](https://img.shields.io/badge/bitbucket-documentation-orange.svg?
style=flat)](http://lukaszlaszko.bitbucket.io/allocators.git/master)
## Motivation

This repository provides implementation of composable allocators described by [Andrei Alexandrescu](http://erdani.com) on [CppCon 2015](https://www.youtube.com/watch?v=LIb3L4vKZ7U)

Following allocators have been implemented:

* **null_allocator**
* **mallocator**
* **fallback_allocator**
* **stack_allocator**
* **free_list**
* **segregator**
* **bucketizer**
* **affix_allocator**
* **bitmapped_block**

Allocators which maybe supplemented in the future:

* **callback_allocator**
* **traced_allocator**

##Usage

All header files composing on this library are located in **src/include/boost/memory**. It suffices to clone this repository and add **src/include** to the list of include directories minded during compilation of your project. For example look into [CMakeLists.txt from composition sample](https://bitbucket.org/lukaszlaszko/allocators/raw/HEAD/samples/composition/CMakeLists.txt).

Implemented allocators then can be used in the code after inclusion of **boost/memory.hpp** header:
    
    #include <boost/memory.hpp>
    ...

    using namespace boost::memory;

    stack_allocator<1024> allocator;
    auto block = allocator.allocate(512);
    ...

The library defines replacement operators [new](http://www.cplusplus.com/reference/new/operator%20new/), [new array](http://www.cplusplus.com/reference/new/operator%20new[]/), [delete](http://www.cplusplus.com/reference/new/operator%20delete/) and [delete array](http://www.cplusplus.com/reference/new/operator%20delete[]/). All of them are activated when **boost/memory/operators.hpp** is included. 
Behavior of the operators can be controlled with macros:

* **DEFINE_ALLOCATOR** which should be instantiated in the declaration space of the main source file of the application in which we wish to replace default **new** and **delete** operators with our own, using supplied compassable allocator as a replacement for the default c++ allocation strategy. Should we trace use of the custom allocator through the overridden operator **DEFINE_ALLOCATOR_WITH_TRACE** macro has to be used instead.

* **INIT_ALLOCATOR** this macro should be instantiated at the very start of main function of every thread in the application. The will properly instantiated dependent allocator. Until instantiated, **mallocator** is used for allocations/deallocations through **new**/**delete** in that thread. **TEARDOWN_ALLOCATOR** can be used to explicitly teardown the allocator used in a thread.   

##Build

The library is purely header based, as such it doesn't require compilation for use in third party project. Thus use of c++ 14 compatible compiler is necessary. 

In order to build unit tests and samples, cmake (3.2 or newer), internet access and all google tests framework prerequisites are required. Any C++ 14 compatible compiler should do, tho the project has been successfully build and tested only on OS X (both Xcode and clang) and Linux (Ubuntu 16.04) with clang 3.9.

To build:

1. Clone this repository

        $ git clone https://bitbucket.org/lukaszlaszko/allocators.git 

2. Configure with cmake
    
        $ mkdir bin
        $ cd bin
        $ cmake ..

3. Build with cmake

        $ cmake --build . --target all

4. Run unit tests

        $ ctest --verbose

Latest bitbucket build results can be observed under this [link](https://bitbucket.org/lukaszlaszko/allocators/addon/pipelines/home#!/).

## Conan package

[Conan](https://docs.conan.io/en/latest) is an opena source package manager for C/C++. [Bintray shadow](https://bintray.com/lukaszlaszko/shadow)
repository provides latest redistributable package with project artefacts. In order to use it in your cmake based project:

1. Add following block to root `CMakeLists.txt` of your project:

    ```cmake
    if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
        include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
        conan_basic_setup()
    else()
        message(WARNING "The file conanbuildinfo.cmake doesn't exist, you have to run conan install first")
    endif()
    ```
    
2. Add `conanfile.txt` side by side with your top level `CMakeLists.txt`:

    ```text
    [requires]
    di/1.0@shadow/stable
    
    [generators]
    cmake
    ```   
    
4. Add `shadow` to your list of conan remotes:

    ```
    $ conan remote add shadow https://api.bintray.com/conan/lukaszlaszko/shadow
    ``` 
    
3. Install conan dependencies into your cmake build directory:

    ```
    $ conan install . -if <build dir>
    ```
    
    if for whatever reason installation of binary package fails, add `--build allocators` flag to the above. This will perform install the source package and compile all necessary modules.      
    
4. To link against libraries provided by the package, either add:

    ```cmake
    target_link_libraries([..] ${CONAN_LIBS})
    ``` 
    
    for your target. Or specifically:
    
    ```cmake
    target_link_libraries([..] ${CONAN_LIBS_DI})
    ```    
    
5. Reload cmake configuration.

## Troubleshooting

If during compilation with GCC > 5 error like this pops up:
```
undefined reference to `testing::internal::GetBoolAssertionFailureMessage[abi:cxx11](testing::AssertionResult const&, char const*, char const*, char const*)
```

make sure conan profile used for gtest installation is configured to use `libstdc++11`. This can be done either by setting **compiler.libcxx=libstdc++11** in `~/.conan/profiles/default` or by specifying it from command line.



Reference - https://docs.conan.io/en/latest/howtos/manage_gcc_abi.html  
